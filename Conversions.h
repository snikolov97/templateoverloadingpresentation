#pragma once
#include <vector>
#include <functional>
#include <algorithm>

using std::vector;
using std::function;

////////////////  CONVERSIONS  ////////////////
template <typename DST, typename SRC>
void Convert(DST& out, const SRC& in) = delete;

template <typename DST, typename SRC>
DST Convert(const SRC& in) = delete;


//////// CONTAINERS ////////

// VIA CONVERSION //
template <typename DST, typename SRC>
typename std::enable_if_t<!std::is_constructible<DST, const SRC&>::value, void>
Convert(vector<DST>& out, const vector<SRC>& in) {
	out.resize(in.size());
	int i = 0;
	for (const auto& elem : in) {
		Convert<DST, SRC>(out[i++], elem);
	}
}

// VIA CONSTRUCTION //
template <typename DST, typename SRC>
typename std::enable_if_t<std::is_constructible<DST, const SRC&>::value> // default return type is void
Convert(vector<DST>& out, const vector<SRC>& in) {
	out.assign(in.begin(), in.end());
}

// VIA LAMBDA //
template <typename DST, typename SRC>
void Convert(vector<DST>& out, const vector<SRC>& in, function<DST(const SRC&)> convertFunc) {
	out.resize(in.size());
	std::transform(in.begin(), in.end(), out.begin(), convertFunc);
}

template <typename DST, typename SRC>
vector<DST> Convert(const vector<SRC>& in)
{
	vector<DST> out;
	Convert<DST, SRC>(out, in);
	return out;
}
