#pragma once
#include "Conversions.h"
#include <string>

// example classes

// Converted from D
struct A {
	std::string m_string;
	double m_double;
	long m_long;
};

// Converts to A
struct D {
	char m_char;
	float m_float;
	int m_int;
};

// Passed to ACs constructor
struct DC {
	char m_char;
	float m_float;
	int m_int;
};

// Constructed from struct DC
struct AC {
	AC() = default;
	AC(const DC& param) : m_double(param.m_float), m_long(param.m_int) {
		m_string = param.m_char;
	}

	std::string m_string;
	double m_double;
	long m_long;
};

// Old conversions

void FromDtoA(A& out, const D& in) {
	out.m_string = in.m_char;
	out.m_double = in.m_float;
	out.m_long = in.m_int;
}

A FromDtoA(const D& in) {
	A out;
	FromDtoA(out, in);
	return out;
}

// SPECIALIZATIONS

template<>
void Convert(A& out, const D& in) {
	out.m_string = in.m_char;
	out.m_double = in.m_float;
	out.m_long = in.m_int;
}

template<>
A Convert(const D& in) {
	A out;
	Convert(out, in);
	return out;
}
