#include "pch.h"
#include <iostream>
#include "SampleClasses.h"
#include "Conversions.h"

#include <vector>
#include <functional>
#include <chrono>

using std::vector;
using std::function;
using namespace std::chrono;

// utils

template <typename Container>
void printContainer(Container c) {
	for (const auto& elem : c) {
		std::cout << elem.m_data << ' ';
	} std::cout << '\n';
}

int main()
{
	// Conversion
	vector<A> Convert_Output;
	vector<D> Convert_Params;

	// Construction
	vector<AC> Construct_Output;
	vector<DC> Construct_Params;

	// The old ways
	// code duplication
	// missing out on optimizations

	// tested for 100k elements

	// for convertibles
	// bad ~1.5m microsec
	for (const auto& elem : Convert_Params)	{
		Convert_Output.push_back(FromDtoA(elem));
	}
	
	// better ~730k microsec
	Convert_Output.reserve(Convert_Params.size());
	for (const auto& elem : Convert_Params)	{
		Convert_Output.emplace_back(FromDtoA(elem));
	}

	// ok ~160k microsec
	Convert_Output.resize(Convert_Params.size());
	int i = 0;
	for (const auto& elem : Convert_Params)	{
		FromDtoA(Convert_Output[i++], elem);
	}

	// for constructibles
	// bad ~1.2m microsec
	for (const auto& elem : Construct_Params) {
		Construct_Output.push_back(elem);
	}

	// better ~220k microsec
	Construct_Output.reserve(Construct_Params.size());
	for (const auto& elem : Construct_Params) {
		Construct_Output.emplace_back(elem);
	}

	// ok ~140k microsec
	Construct_Output.assign(Construct_Params.begin(), Construct_Params.end());


	// can be moved to a function but how to generalize the conversion functions?

	// The new ways

	Convert(Convert_Output, Convert_Params);
	Convert(Construct_Output, Construct_Params);

	Convert<A, D>(Convert_Output, Convert_Params, [](const D& d) { return FromDtoA(d); });
	Convert<A, D>(Convert_Output, Convert_Params, static_cast<A (*)(const D&)>(FromDtoA));

	// testing grounds

	Convert_Output.clear();
	Convert_Params.clear();
	Construct_Output.clear();
	Construct_Params.clear();


	high_resolution_clock::time_point t1 = high_resolution_clock::now();

	high_resolution_clock::time_point t2 = high_resolution_clock::now();

	std::cout << duration_cast<microseconds>(t2 - t1).count();
}