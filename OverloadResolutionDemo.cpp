#include "pch.h"
#include <iostream>

using std::cout;

template <typename T, typename U>                      // A
void foo(T arg1, U arg2) {
	cout << "Generic Template\n";
}

template<>                                             // B
void foo<int, int>(int arg1, int arg2) {
	cout << "Template Specialization\n";
}

template <typename T>                                  // C
void foo(T arg1, int arg2) {
	cout << "\"Less-templated\" Overload\n";
}

template<>                                             // D
void foo<int>(int arg1, int arg2) {
	cout << "\"Less-templated\" Specialization\n";
}

void foo(int arg1, int arg2) {                         // E
	cout << "Non-template Overload\n";
}

int main()
{
	foo<int, int>(10, 10);       // 1
	foo(10.0, 10.0);             // 2
	foo(new int, 5);             // 3
	foo(10, 10);                 // 4
	foo<int>(10, 10);            // 5
	foo(10.0, 10);               // 6
	foo(5, new int);             // 7
}